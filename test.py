import graphene
from flask_graphql import GraphQLView
from alembic.ddl import mysql
from flask_cognito import CognitoAuth, cognito_auth_required, current_user, current_cognito_jwt
from flask import Flask
from flask import request
import sqlite3
import hashlib
import collections
import itertools
import copy
from polyglot.detect import Detector
import speech_recognition as sr
import shutil
import os
from gtts import gTTS
from flask import send_file

from pydub.silence import split_on_silence
from pydub import AudioSegment
from googletrans import Translator

translator = Translator()
import pendulum
import os
import pymysql
import pymysqlreplication
import singer
import singer.metrics as metrics
import singer.schema
from singer import metadata
from singer.schema import Schema
from singer.catalog import Catalog, CatalogEntry
import tap_mysql.sync_strategies.binlog as binlog

import tap_mysql.sync_strategies.incremental as incremental
from tap_mysql.connection import connect_with_backoff, MySQLConnection
from dotenv import load_dotenv
from tap_google_sheets.client import GoogleClient
import boto3
import sys
from flask import Flask

app = Flask(__name__)


@app.route('/')
def test():
    return "welcome In Test !"


if __name__ == '__main__':
    app.run()
